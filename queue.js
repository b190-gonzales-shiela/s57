let collection=[];

// program to implement queue data structure

class Queue {
    constructor() {
        this.collection = [];
    }
    
    // add element to the queue
    enqueue(john) {
        return this.collection.push(john);
    }
    
    // remove element from the queue
    dequeue() {
        if(this.collection.length > 0) {
            return this.collection.shift();
        }
    }
    
    // view the last element
    peek() {
        return this.collection[this.collection.length - 1];
    }
    
    // check if the queue is empty
    isEmpty(){
       return this.collection.length == 0;
    }
   
    // the size of the queue
    size(){
        return this.collection.length;
    }
 
    // empty the queue
    
}

let queue = new Queue();
queue.enqueue('John');
queue.enqueue('Jane');
queue.enqueue('Bob');
queue.enqueue('Cherry');
console.log(queue.collection);

queue.dequeue();
console.log(queue.print);

console.log(queue.peek());

console.log(queue.isEmpty());

console.log(queue.size());

console.log(queue.collection);


module.exports = {
    collection,
    print,
    enqueue,
    dequeue,
    front,
    size,
    isEmpty
};